#------------------------------------------------------------------------------
# Declare source files for modelbuilder

include(${PARAVIEW_USE_FILE})

# Headers that are not Qt subclasses
set(modelbuilder_HDRS
  mbPluginConfigLoader.h
  mbMenuBuilder.h
  mbObjectFactory.h
  mbReaderFactory.h

  ${CMAKE_CURRENT_BINARY_DIR}/mbVersion.h
)

# Headers that are Qt subclasses (and thus need moc run on them)
set(modelbuilder_MOC_HDRS
  mbMainWindow.h
  mbTestEventPlayer.h
  mbTestEventTranslator.h
)

# Implementation files.
set(modelbuilder_SRCS
  mbPluginConfigLoader.cxx
  mbMainWindow.cxx
  mbMenuBuilder.cxx
  mbObjectFactory.cxx
  mbReaderFactory.cxx
  mbTestEventPlayer.cxx
  mbTestEventTranslator.cxx
)

# A feature that exposes ParaView's main window events (show, close, drag, etc.)
# was introduced just after the tagging of 5.6.0. These signals allow us to
# prompt the user to save modified resources prior to closing the application.
# To make this feature available while not requiring the use an as-yet untagged
# ParaView, we set a local compiler definition to enable this feature if the
# detected ParaView contains the features we need.
#
# TODO: In the future, ParaView will have a means of differentiating between the
# latest tagged branch and master. When that happens, we can replace this logic
# with a ParaView version check to see if our ParaView is greater than 5.6.0.
# Until then, we simply look for the header file associated with the feature we
# want.
find_file(main_window_event_manager_hdr
  NAMES pqMainWindowEventManager.h
  PATHS ${PARAVIEW_INCLUDE_DIRS}
  NO_DEFAULT_PATH)
if (main_window_event_manager_hdr)
  set_source_files_properties(mbMainWindow.cxx
    PROPERTIES COMPILE_DEFINITIONS "HAS_MAIN_WINDOW_EVENT_MANAGER")
endif()


# Qt UI files
set(modelbuilder_UI_SRCS
  ui/mbMainWindow.ui
  ui/mbFileMenu.ui
)

# Qt resource files
set(modelbuilder_RSRCS
  resource/mbResource.qrc
)

#------------------------------------------------------------------------------
# Add extra library containing custom code for the client.
include(ParaViewQt)
pv_find_package_qt(qt_targets REQUIRED QUIET QT5_COMPONENTS Widgets)
pv_qt_wrap_cpp(MOC_BUILT_SOURCES ${modelbuilder_MOC_HDRS})
pv_qt_wrap_ui(UI_BUILT_SOURCES ${modelbuilder_UI_SRCS})
pv_qt_add_resources(QT_BUILT_RESOURCES ${modelbuilder_RSRCS})
include_directories(
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${CMAKE_CURRENT_BINARY_DIR}
)
# ${CMAKE_CURRENT_SOURCE_DIR}/Documentation)

configure_file(
  ${PROJECT_SOURCE_DIR}/cmake/Version.h.in
  ${CMAKE_CURRENT_BINARY_DIR}/mbVersion.h
)

configure_file(
  mbTestData.h.in
  ${CMAKE_CURRENT_BINARY_DIR}/mbTestData.h
)

set(modelbuilder_SOURCE_FILES
  ${modelbuilder_SRCS}
  ${modelbuilder_HDRS}
  ${modelbuilder_MOC_HDRS}
  ${MOC_BUILT_SOURCES}
  ${UI_BUILT_SOURCES}
  ${QT_BUILT_RESOURCES}
)

#------------------------------------------------------------------------------
# ParaView applications provides a mechanism to add gui xmls from modules.
# This is done by defining variables named ${vtk-module}_PARAVIEW_GUI_XMLS in
# the module.cmake file for the modules pointing to the paths for the
# GUI-xmls.  We process those here.
set (application_gui_xmls
 "${CMAKE_CURRENT_SOURCE_DIR}/postprocessing-filters.xml"
 "${CMAKE_CURRENT_SOURCE_DIR}/postprocessing-sources.xml"
)
foreach (module IN LISTS VTK_MODULES_ENABLED)
  get_property(gui_xml GLOBAL PROPERTY ${module}_PARAVIEW_GUI_XMLS)
  if (gui_xml)
    foreach(xml IN LISTS gui_xml)
      list(APPEND application_gui_xmls ${xml})
    endforeach()
  endif()
endforeach()

#if (PARAVIEW_ENABLE_EMBEDDED_DOCUMENTATION)
#  #------------------------------------------------------------------------------
#  # Build Online-Help (aka Embedded Help) for the modelbuilder application.
#  # This is done after the above piece of code that sets the application_gui_xmls
#  # variable. Documentation/CMakeLists.txt depends on it.
#  add_subdirectory(Documentation)
#endif()

set(title "modelbuilder ${cmb_version} ${cmb_build_architecture}-bit")
#------------------------------------------------------------------------------
# Build the client
build_paraview_client(modelbuilder
  APPLICATION_NAME "modelbuilder"
  TITLE "${title}"
  ORGANIZATION  "modelbuilder"
  VERSION_MAJOR ${cmb_version_major}
  VERSION_MINOR ${cmb_version_minor}
  VERSION_PATCH ${cmb_version_patch}
  PVMAIN_WINDOW mbMainWindow
  PVMAIN_WINDOW_INCLUDE mbMainWindow.h
  BUNDLE_ICON   "${CMAKE_CURRENT_SOURCE_DIR}/modelbuilder.icns"
  APPLICATION_ICON  "${CMAKE_CURRENT_SOURCE_DIR}/modelbuilder.ico"
  GUI_CONFIGURATION_XMLS ${application_gui_xmls}
  SOURCES ${modelbuilder_SOURCE_FILES}
  INSTALL_RUNTIME_DIR "${VTK_INSTALL_RUNTIME_DIR}"
  INSTALL_LIBRARY_DIR "${VTK_INSTALL_LIBRARY_DIR}"
  INSTALL_ARCHIVE_DIR "${VTK_INSTALL_ARCHIVE_DIR}"
  # Disable this until we have a preference to prevent it,
  # otherwise it interferes with debugging:
  # SPLASH_IMAGE "${CMAKE_CURRENT_SOURCE_DIR}/resource/splash.png"
)

target_link_libraries(modelbuilder
  LINK_PRIVATE
    ${qt_targets}
    cmbPostProcessingModePlugin
    vtkPVServerManagerDefault
    )
target_include_directories(modelbuilder
  PUBLIC
    $<BUILD_INTERFACE:${cmb_SOURCE_DIR}>
    $<BUILD_INTERFACE:${cmb_BINARY_DIR}>
)

#if (PARAVIEW_ENABLE_EMBEDDED_DOCUMENTATION)
#  # Link against the documentation module.
#  target_link_libraries(modelbuilder
#    LINK_PRIVATE vtkParaViewDocumentation)
#endif()

# link enabled plugins if not building in shared library mode and
# add dependecies to linked python modules These are non-empty only when
# building statically.
if (PARAVIEW_ENABLE_PYTHON)
  target_link_libraries(modelbuilder
    LINK_PRIVATE
      vtkUtilitiesPythonInitializer
  )
endif()

# If ParaView was built statically, then vtkPVStaticPluginsInit was also
# created. We can branch off of the existence of this target to determine if we
# also need to support static plugins.
if(TARGET vtkPVStaticPluginsInit)
  target_link_libraries(modelbuilder
    LINK_PRIVATE
      vtkPVStaticPluginsInit
  )
endif()

# Plugins can be directly linked into ParaView-based applications, or they can
# be identified and loaded at runtime. We would prefer to defer the plugin
# selection process to runtime, but we have found the former option to be more
# reliable.
set(MODELBUILDER_DIRECTLY_LINK_TO_SMTK ON CACHE BOOL "Directly link SMTK plugins into ModelBuilder")

set(MODELBUILDER_PLUGINS "" CACHE STRING "Plugins to build into ModelBuilder.")

if (MODELBUILDER_DIRECTLY_LINK_TO_SMTK)
  # If we are directly linking to SMTK, then SMTK is a required dependency.
  find_package(smtk REQUIRED)

  include(SMTKPluginMacros)

  # In between SMTK 3.0.1 and 3.1.0, a feature was introduced to SMTK that
  # enabled it to generate a plugin library from the contents of SMTK_PLUGINS, a
  # global property that is a list of all SMTK plugin targets (both local to
  # this project and imported from other projects). We check if this function
  # exists in the SMTK we found. If it doesn't, we fall back to directly linking
  # the plugins defined in SMTK.
  if (COMMAND generate_smtk_plugin_library)

    # For each plugin target listed by the user, locate the project. If the
    # project uses SMTK's plugin macros to define its plugins, this will append
    # the project's plugin targets to the global property SMTK_PLUGINS.
    list(REMOVE_DUPLICATES MODELBUILDER_PLUGINS)
    foreach(plugin ${MODELBUILDER_PLUGINS})
      find_package(${plugin})
    endforeach()

    # Generate the target PluginsForModelBuilder, along with its header files
    # (defined in ${PluginsForModelBuilder_HEADERS}).
    list(REMOVE_DUPLICATES SMTK_PLUGINS)
    generate_smtk_plugin_library(PluginsForModelBuilder)

    set_target_properties(PluginsForModelBuilder PROPERTIES CXX_VISIBILITY_PRESET hidden)
    install(TARGETS PluginsForModelBuilder
      RUNTIME DESTINATION "${VTK_INSTALL_RUNTIME_DIR}"
      LIBRARY DESTINATION "${VTK_INSTALL_LIBRARY_DIR}"
      ARCHIVE DESTINATION "${VTK_INSTALL_ARCHIVE_DIR}"
    )

    list(APPEND modelbuilder_HDRS
      ${PluginsForModelBuilder_HEADERS})

    target_link_libraries(modelbuilder
      LINK_PRIVATE
      smtkCore
      PluginsForModelBuilder
    )

  # There is also a bit of code that must be present in ModelBuilder to load
  # the identified plugins. We enable it here.
  set_property(SOURCE mbMainWindow.cxx APPEND PROPERTY COMPILE_DEFINITIONS
    "DIRECTLY_LINK_TO_PLUGINS")

  else()

    # If "generate_smtk_plugin_library" doesn't exist, then the version of SMTK
    # we found has the target "smtkDefaultPlugins" that allows the application
    # to direclty link SMTK's plugins to it.
    target_link_libraries(modelbuilder
      LINK_PRIVATE
      smtkCore
      smtkDefaultPlugins
      )
  endif ()

else()

  # We are not directly linking to SMTK, so SMTK is no longer a required
  # dependency. If we find it, we will generate a custom .plugins file for
  # ModelBuilder to use during testing and for installation.
  find_package(smtk)

  if (smtk_FOUND)

    # For each plugin target listed by the user, locate the project. If the
    # project uses SMTK's plugin macros to define its plugins, this will append
    # the project's plugin targets to the global property SMTK_PLUGINS.
    list(REMOVE_DUPLICATES MODELBUILDER_PLUGINS)
    foreach(plugin ${MODELBUILDER_PLUGINS})
      find_package(${plugin})
    endforeach()

    # Create a custom .plugins xml file that contains all of the plugins we have
    # found. This file will be used to load plugins during testing.
    if (WIN32)
      set(plugins_file ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/plugins/.plugins)
    else()
      set(plugins_file
        ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/modelbuilder-${cmb_version}/plugins/.plugins)
    endif()

    set(plugin_dir ${CMAKE_INSTALL_PREFIX}/${PARAVIEW_INSTALL_PLUGINS_DIR})
    if (APPLE AND NOT PARAVIEW_DO_UNIX_STYLE_INSTALLS)
      set(plugin_dir "${MACOSX_APP_INSTALL_PREFIX}/modelbuilder.app/Contents/Plugins")
    endif ()

    include(SMTKPluginMacros)

    # In between SMTK 3.0.1 and 3.1.0, a feature was introduced to SMTK that
    # enabled it to generate plugin configuration files for ParaView-branded
    # consuming applications. This logic also allowed the plugin creator to flag
    # whether or not the plugin should be automatically loaded (for required
    # plugins). To preserve backwards-compatibility, we key off of the existence
    # of this CMake function and fall back to generating our own configuration
    # file if it does not exist.

    if (COMMAND generate_smtk_plugin_config_file)
      list(REMOVE_DUPLICATES SMTK_PLUGINS)
      generate_smtk_plugin_config_file(${plugins_file})
    else()
      set(contents "<?xml version=\"1.0\"?>\n<Plugins>\n</Plugins>\n")
      list(REMOVE_DUPLICATES SMTK_PLUGINS)
      foreach (plugin IN LISTS SMTK_PLUGINS)
        get_property(${plugin}_location TARGET ${plugin} PROPERTY LOCATION)
        file(RELATIVE_PATH ${plugin}_location ${plugin_dir} ${${plugin}_location})
        set(plugin_directive
          "  <Plugin name=\"${plugin}\" filename=\"${${plugin}_location}\" auto_load=\"1\" />\n")
        string(REPLACE "</Plugins>" "${plugin_directive}</Plugins>" contents "${contents}")
      endforeach ()

      file(WRITE "${plugins_file}" "${contents}")
    endif()

    install(
      FILES       "${plugins_file}"
      DESTINATION "${plugin_dir}"
      RENAME      ".plugins")
  endif()
endif()

if (cmb_enable_testing)
  add_subdirectory(testing)
endif ()

#------------------------------------------------------------------------------
if (APPLE AND NOT PARAVIEW_DO_UNIX_STYLE_INSTALLS)

  # Use a custom plist file, since the default one provided by CMake does not
  # enable high resolution graphics. In the future, we can augment this plist
  # file to include default file extensions that can be opened by modelbuilder.
  # See ParaView's info.plist as an example.
  configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/resource/MacOSXBundleInfo.plist.in.in"
    "${CMAKE_CURRENT_BINARY_DIR}/MacOSXBundleInfo.plist.in"
    @ONLY
    )
  set_target_properties(modelbuilder
    PROPERTIES MACOSX_BUNDLE_INFO_PLIST ${CMAKE_CURRENT_BINARY_DIR}/MacOSXBundleInfo.plist.in)

  # For Macs, we add install rule to package everything that's built into a single
  # App. Look at the explanation of MACOSX_APP_INSTALL_PREFIX in the top-level
  # CMakeLists.txt file for details.

  # add install rules to generate the App bundle.
  install(CODE "
   include(\"${ParaView_CMAKE_DIR}/ParaViewBrandingInstallApp.cmake\")

   #fillup bundle with all the libraries and plugins.
   cleanup_bundle(
     \$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}/${VTK_INSTALL_RUNTIME_DIR}/modelbuilder.app/Contents/MacOS/modelbuilder
     \$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}/${VTK_INSTALL_RUNTIME_DIR}/modelbuilder.app
     \$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}/${VTK_INSTALL_LIBRARY_DIR}
     \$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}/${PARAVIEW_INSTALL_PLUGINS_DIR}
     \$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}/${VTK_INSTALL_DATA_DIR})

   # Place the App at the requested location.
   file(INSTALL DESTINATION \"${MACOSX_APP_INSTALL_PREFIX}\"
        TYPE DIRECTORY FILES
          \"\$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}/${VTK_INSTALL_RUNTIME_DIR}/modelbuilder.app\"
        USE_SOURCE_PERMISSIONS)
   "
   COMPONENT Runtime)
elseif (APPLE AND PARAVIEW_DO_UNIX_STYLE_INSTALLS)
  # This is a unix style install on OsX. Purge the bundle.
  install(CODE
    "
      include(\"${ParaView_CMAKE_DIR}/ParaViewBrandingInstallApp.cmake\")
      convert_bundle_to_executable(
          \$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}/${VTK_INSTALL_RUNTIME_DIR}/modelbuilder.app/Contents/MacOS/modelbuilder
          \$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}/${VTK_INSTALL_RUNTIME_DIR}/modelbuilder.app
          \$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}/${VTK_INSTALL_RUNTIME_DIR})
    "
    COMPONENT Runtime)
elseif (UNIX)
  configure_file(
    modelbuilder.desktop.in
    "${CMAKE_CURRENT_BINARY_DIR}/modelbuilder.desktop"
    @ONLY)
  install(
    FILES       "${CMAKE_CURRENT_BINARY_DIR}/modelbuilder.desktop"
    DESTINATION share/applications
    COMPONENT   runtime)
  foreach (iconsize 22x22 32x32 96x96)
    install(
      FILES       "resource/modelbuilder-${iconsize}.png"
      DESTINATION "share/icons/hicolor/${iconsize}/apps"
      RENAME      modelbuilder.png
      COMPONENT   runtime)
  endforeach ()
  install(
    FILES       resource/modelbuilder.appdata.xml
    DESTINATION share/appdata
    COMPONENT   runtime)
endif()
