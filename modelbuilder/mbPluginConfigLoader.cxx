//=============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=============================================================================

#include "mbPluginConfigLoader.h"

#include "mbVersion.h"

#include "vtkPResourceFileLocator.h"
#include "vtkPVPluginTracker.h"
#include "vtkProcessModule.h"
#include "vtkVersion.h"

#include <vtksys/String.hxx>
#include <vtksys/SystemTools.hxx>

namespace
{
/**
 * Locate a plugin library or a config file anchored at standard locations
 * for locating plugins.
 */
std::string locatePluginConfigFile(const char* plugin)
{
  auto pm = vtkProcessModule::GetProcessModule();
  // Make sure we can get the options before going further
  if (pm == NULL)
  {
    return std::string();
  }

  const std::string exe_dir = pm->GetSelfDir();
  const std::string vtklib = vtkGetLibraryPathForSymbol(GetVTKVersion);

  std::vector<std::string> prefixes = {
    std::string("modelbuilder-" CMB_VERSION "/plugins/") + plugin,
    std::string("modelbuilder-" CMB_VERSION "/plugins/"),
    std::string("lib/modelbuilder-" CMB_VERSION "/plugins/") + plugin,
    std::string("lib/modelbuilder-" CMB_VERSION "/plugins/"),

#if defined(__APPLE__)
    // needed for Apps
    std::string("Plugins/") + plugin,
    std::string("Plugins/"),
#elif defined(_WIN32)
    std::string("plugins/") + plugin,
    std::string("plugins/"),
#endif
    std::string()
  };

  const std::string landmark = plugin;

  vtkNew<vtkPResourceFileLocator> locator;

  // Now, try the prefixes we so careful put together.
  if (!vtklib.empty())
  {
    auto pluginpath =
      locator->Locate(vtksys::SystemTools::GetFilenamePath(vtklib), prefixes, landmark);
    if (!pluginpath.empty())
    {
      return pluginpath + "/" + landmark;
    }
  }
  if (!exe_dir.empty())
  {
    auto pluginpath = locator->Locate(exe_dir, prefixes, landmark);
    if (!pluginpath.empty())
    {
      return pluginpath + "/" + landmark;
    }
  }

  return std::string();
}
}

void mbPluginConfigLoader::loadPluginConfigFile()
{
  std::string pluginFileLocation = locatePluginConfigFile(".plugins");
  vtkPVPluginTracker::GetInstance()->LoadPluginConfigurationXML(pluginFileLocation.c_str());
}
