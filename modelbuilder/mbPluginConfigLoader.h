//=============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#ifndef modelbuilder_mbPluginConfigLoader_h
#define modelbuilder_mbPluginConfigLoader_h

class mbPluginConfigLoader
{
public:
  /**\brief Load the plugin config file.
    *
    */
  static void loadPluginConfigFile();
};

#endif
