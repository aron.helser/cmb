//=============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#ifndef modelbuilder_mbTestEventPlayer_h
#define modelbuilder_mbTestEventPlayer_h

#include "pqWidgetEventPlayer.h"

/**\brief Subclass of pqWidgetEventPlayer that handles recorded file dialog events.
  *
  * \sa pqEventPlayer
  */
class mbTestEventPlayer : public pqWidgetEventPlayer
{
  Q_OBJECT
public:
  mbTestEventPlayer(QObject* p = 0);

  bool playEvent(
    QObject* Object, const QString& Command, const QString& Arguments, bool& Error) override;

private:
  mbTestEventPlayer(const mbTestEventPlayer&) = delete;
  mbTestEventPlayer& operator=(const mbTestEventPlayer&) = delete;
};

#endif
