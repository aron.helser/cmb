####### MTK Documentation

## Reference Documentation
#
# If we have doxygen, create reference API documentation for CMB.
#
# This may one day generate documentation for third-party libraries
# (e.g., SMTK) which are referenced by CMB's documentation.
#
if(DOXYGEN_FOUND)
  file(MAKE_DIRECTORY "${cmb_BINARY_DIR}/doc/reference")
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/cmb.doxyfile.in
    ${CMAKE_CURRENT_BINARY_DIR}/cmb.doxyfile
    @ONLY
  )

  add_custom_command(
    OUTPUT ${cmb_BINARY_DIR}/doc/reference/cmb.tags
    COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/cmb.doxyfile
    WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/reference"
    DEPENDS
      "${CMAKE_CURRENT_BINARY_DIR}/cmb.doxyfile"
      COMMENT "Generating CMB API documentation with Doxygen" VERBATIM
  )
  add_custom_target(doc
    DEPENDS ${cmb_BINARY_DIR}/doc/reference/cmb.tags
  )
endif(DOXYGEN_FOUND)

## End-user Documentation
#
# If we have rst2html, create the user's guide for CMB
# as an HTML document.

# Define a macro for processing reStructuredText files
# if docutils were found.
if (SPHINX_FOUND)
  function(smtk_add_doc sphinxTargetName)
    set(options)
    set(oneValueArgs DESTINATION SOURCE_DIR BUILD_DIR)
    set(multiValueArgs DEPENDS FIGURES)
    cmake_parse_arguments(sphinx "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
    if (NOT sphinx_SOURCE_DIR)
      set(sphinx_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}") # Reasonable default
    endif()
    if (NOT sphinx_BUILD_DIR)
      set(sphinx_BUILD_DIR "${CMAKE_CURRENT_BINARY_DIR}") # Reasonable default
    endif()
    # Generate HTML version of docs
    set(sphinx_HTML_TOP "${CMAKE_CURRENT_BINARY_DIR}/${sphinx_BUILD_DIR}/html/index.html")
    add_custom_command(
      OUTPUT "${sphinx_HTML_TOP}"
      DEPENDS
        ${CMAKE_CURRENT_SOURCE_DIR}/conf.py
        ${sphinx_DEPENDS}
        ${figureList}
        COMMAND ${SPHINX_EXECUTABLE}
      ARGS
        -b html
        "${sphinx_SOURCE_DIR}"
        "${sphinx_BUILD_DIR}/html"
      WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
      COMMENT "Generating HTML for ${sphinxTargetName}"
    )
    add_custom_target(doc-${sphinxTargetName} DEPENDS "${sphinx_HTML_TOP}")
    if (sphinx_DESTINATION)
      install(
        DIRECTORY "${sphinx_BUILD_DIR}/html/"
        DESTINATION "${sphinx_DESTINATION}"
        COMPONENT Development)
      install(
        FILES ${figureList}
        DESTINATION "${sphinx_DESTINATION}/figures"
        COMPONENT Development)
    endif()
  endfunction()

  set(CMB_USERGUIDE_DOCS
    index.rst
    userguide/index.rst
  )
  if (DOXYGEN_FOUND)
    # Need doxygen docs built if possible
    list(APPEND CMB_USERGUIDE_DOCS
      ${cmb_BINARY_DIR}/doc/reference/cmb.tags)
  endif()

  set(CMB_USERGUIDE_FIGS
  )

  # Add the top-level reStructuredText file.
  # All others are included by it.
  smtk_add_doc(userguide
    DEPENDS ${CMB_USERGUIDE_DOCS}
    #FIGURES ${CMB_USERGUIDE_FIGS}
    BUILD_DIR ${CMAKE_CURRENT_BINARY_DIR}/user
    DESTINATION share/doc/cmb/userguide
  )
endif()
