# CMB 6.2 Release Notes
CMB 6.2 is a minor release with new features.  **Note any non-backward compatible changes are in bold italics**. See also [CMB 6.1 Release Notes](cmb-6.1.md).


## CMB 6.2 Dependancies
* CMB 6.1 is based on SMTK 3.2 - For changes made to SMTK 3.2 please see the release notes in SMTK
* CMB 6.2 is based on ParaView 5.6

## Added a Filter for Readers for Post-Processing
Readers for post-processing can now be hidden if post-processing is not enabled
## Added Multi-Block Inspector
The inspector is now available when post-processing is enabled
