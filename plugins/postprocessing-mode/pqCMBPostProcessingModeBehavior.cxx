//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "plugins/postprocessing-mode/pqCMBPostProcessingModeBehavior.h"

// Client side
#include "pqApplicationCore.h"
#include "pqCoreUtilities.h"
#include "pqInterfaceTracker.h"
#include "pqOutputPort.h"
#include "pqPVApplicationCore.h"
#include "pqParaViewMenuBuilders.h"

// Qt
#include <QAction>
#include <QDir>
#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QMetaObject>
#include <QTimer>
#include <QToolBar>
#include <QWidget>

// Qt generated UI
#include "ui_pqCMBPostProcessingModeBehavior.h"

#include <iostream>
#include <vector>

static pqCMBPostProcessingModeBehavior* s_postMode = nullptr;

class pqCMBPostProcessingModeBehavior::pqInternal
{
public:
  Ui::pqCMBPostProcessingModeBehavior Actions;
  QAction* ModeAction;
  QWidget ActionsOwner;
};

pqCMBPostProcessingModeBehavior::pqCMBPostProcessingModeBehavior(QObject* parent)
  : Superclass(parent)
{
  m_p = new pqInternal;
  m_p->Actions.setupUi(&m_p->ActionsOwner);

  m_p->ModeAction = m_p->Actions.actionPostProcessingMode;

  if (!s_postMode)
  {
    s_postMode = this;
  }

  // By default, the button is off. Register them in this state.
  this->addAction(m_p->ModeAction);
  this->setExclusive(false);

  QObject::connect(this, SIGNAL(triggered(QAction*)), this, SLOT(switchModes(QAction*)));
  QTimer::singleShot(0, this, SLOT(prepare()));
}

pqCMBPostProcessingModeBehavior::~pqCMBPostProcessingModeBehavior()
{
  if (s_postMode == this)
  {
    s_postMode = nullptr;
  }
}

pqCMBPostProcessingModeBehavior* pqCMBPostProcessingModeBehavior::instance()
{
  return s_postMode;
}

void pqCMBPostProcessingModeBehavior::prepare()
{
  QObject::connect(this, SIGNAL(togglePostProcessingMode(bool)), pqCoreUtilities::mainWidget(),
    SLOT(togglePostProcessingMode(bool)));
}

void pqCMBPostProcessingModeBehavior::switchModes(QAction* a)
{
  emit togglePostProcessingMode(a->isChecked());
}
